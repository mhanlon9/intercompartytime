//
//  MapViewController.h
//  IntercomPartyTime
//
//  Created by Matthew Hanlon on 8/4/16.
//  Copyright © 2016 Q.I. Software. All rights reserved.
//

#import <UIKit/UIKit.h>

static double const kIntercomLatitude = 53.3381985;
static double const kIntercomLongitude = -6.2592576;

@interface MapViewController : UIViewController
@property (strong, readwrite) NSArray* users;

@end
