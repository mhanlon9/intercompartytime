//
//  ViewController.m
//  IntercomPartyTime
//
//  Created by Matthew Hanlon on 8/4/16.
//  Copyright © 2016 Q.I. Software. All rights reserved.
//

#import "ViewController.h"
#import "IPTTableViewCell.h"
#import "UserFinder.h"
#import "MapViewController.h"

static NSString* const kCellIdentifier = @"UserCell";

@interface ViewController ()
@property (strong) UserFinder* finder;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (strong) NSArray* nearbyUsers;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.finder = [[UserFinder alloc] init];
    [self.finder loadUsersFromFile];
    // Sort them by user_id...
    NSArray* users = [self.finder usersWithin:100 ofLatitude:kIntercomLatitude andLongitude:kIntercomLongitude];
    self.nearbyUsers = [users sortedArrayUsingComparator:^NSComparisonResult( id  _Nonnull obj1, id  _Nonnull obj2 ) {
        return ( [obj1[@"user_id"] compare:obj2[@"user_id"]] );
    }];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ( [segue.destinationViewController isKindOfClass:[MapViewController class]] )
    {
        MapViewController* controller = (MapViewController*)segue.destinationViewController;
        controller.users = self.nearbyUsers;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.finder.nearbyUsers.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    IPTTableViewCell* cell = (IPTTableViewCell*)[self.tableView dequeueReusableCellWithIdentifier:kCellIdentifier];
    if ( cell == nil )
    {
        cell = [[IPTTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kCellIdentifier];
    }
    
    [cell configureCellWithUser:[self.nearbyUsers objectAtIndex:indexPath.row]];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70;
}

@end
