//
//  MapViewController.m
//  IntercomPartyTime
//
//  Created by Matthew Hanlon on 8/4/16.
//  Copyright © 2016 Q.I. Software. All rights reserved.
//

#import "MapViewController.h"
#import <MapKit/MapKit.h>

static NSString* const kIntercomTitle = @"Intercom";

@interface MapViewController () <MKMapViewDelegate>
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (nonatomic, strong) NSArray* annotations;
@end

@implementation MapViewController


-(void)viewDidLoad
{
    self.mapView.delegate = self;
    // Add the Intercom Office annotation
    MKPointAnnotation* intercomAnnotation = [[MKPointAnnotation alloc] init];
    CLLocationCoordinate2D intercomCoord = CLLocationCoordinate2DMake( kIntercomLatitude, kIntercomLongitude );
    intercomAnnotation.coordinate = intercomCoord;
    intercomAnnotation.title = kIntercomTitle;
    intercomAnnotation.subtitle = @"Party!";
    [self.mapView addAnnotation:intercomAnnotation];
    
    
    for (NSDictionary* user in self.users)
    {
        MKPointAnnotation* annotation = [[MKPointAnnotation alloc] init];
        double lat = [user[@"latitude"] doubleValue];
        double lng = [user[@"longitude"] doubleValue];
        CLLocationCoordinate2D coord = CLLocationCoordinate2DMake( lat, lng );
        annotation.coordinate = coord;
        annotation.title = user[@"name"];
        annotation.subtitle = [user[@"user_id"] stringValue];
        
        [self.mapView addAnnotation:annotation];
    }
    
    // We'll focus on the 115km around Intercom's office, for kicks
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance( intercomCoord, ( 1000 * 115 ), ( 1000 * 115 ) );
    [self.mapView setRegion:region animated:YES];
}

- ( nullable MKAnnotationView * )mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    if ( [annotation.title isEqualToString:kIntercomTitle] )
    {
        MKPinAnnotationView *pinView = (MKPinAnnotationView*)[mapView dequeueReusableAnnotationViewWithIdentifier:@"IntercomPinView"];
        if ( pinView == nil )
        {
            pinView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"IntercomPinView"];
            pinView.canShowCallout = YES;
            pinView.pinTintColor = [UIColor blueColor];
            pinView.calloutOffset = CGPointMake(0, 32);

            UIImageView* imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"officebuilding.png"]];
            pinView.leftCalloutAccessoryView = imageView;
        }
        pinView.annotation = annotation;
        return pinView;
    }
    return nil;
}

@end
