//
//  IPTTableViewCell.m
//  IntercomPartyTime
//
//  Created by Matthew Hanlon on 8/4/16.
//  Copyright © 2016 Q.I. Software. All rights reserved.
//

#import "IPTTableViewCell.h"

@interface IPTTableViewCell ()

@property (strong, nonatomic) UIView *circleView;
@property (strong, nonatomic) UILabel *userNameLabel;
@property (strong, nonatomic) UILabel *userIDLabel;
@property (strong, nonatomic) UILabel *userDistanceLabel;

@end

@implementation IPTTableViewCell


- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if ( self == [super initWithStyle:style reuseIdentifier:reuseIdentifier] )
    {
        //Instantiate all of the different labels
        self.userNameLabel = [[UILabel alloc] init];
        self.userIDLabel = [[UILabel alloc] init];
        self.userDistanceLabel = [[UILabel alloc] init];
        self.circleView = [[UIView alloc] init];
        
        //Let's setup these different view
        self.circleView.layer.masksToBounds = YES;
        self.circleView.layer.cornerRadius = 5;
        self.circleView.backgroundColor = [UIColor blueColor];
        
        self.userNameLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:18.0];
        self.userIDLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:10.0];
        self.userDistanceLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:12.0];
        
        self.userNameLabel.numberOfLines = 0;
        self.userIDLabel.numberOfLines = 0;
        self.userDistanceLabel.numberOfLines = 0;
        
        for ( UIView *view in @[self.userNameLabel, self.userIDLabel, self.userDistanceLabel, self.circleView] )
        {
            [self.contentView addSubview:view];
            view.translatesAutoresizingMaskIntoConstraints = NO;
        }
        
        NSDictionary *viewDictionary = NSDictionaryOfVariableBindings( _userNameLabel, _userIDLabel, _userDistanceLabel, _circleView );
        
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-20-[_circleView(==10)]-20-[_userNameLabel]-[_userIDLabel]|" options:NSLayoutFormatAlignAllCenterY metrics:nil views:viewDictionary]];
        
        //Setup the vertical layout of the items inside the cell
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-20-[_userNameLabel]-[_userDistanceLabel]" options:NSLayoutFormatAlignAllLeft metrics:nil views:viewDictionary]];
        
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[_userDistanceLabel]-|" options:kNilOptions metrics:nil views:viewDictionary]];
        
        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:_circleView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:_circleView attribute:NSLayoutAttributeWidth multiplier:1 constant:0]];
        
    }
    
    return self;
}


- (void) configureCellWithUser:(NSDictionary *)user
{
    self.userNameLabel.text = user[@"name"];
    self.userIDLabel.text = [NSString stringWithFormat:@"[%@]", user[@"user_id"]];
    self.userDistanceLabel.text = [NSString stringWithFormat:@"%0.2f km", [user[@"distance_km"] doubleValue]];
}

@end
