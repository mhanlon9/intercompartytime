//
//  UserFinder.m
//  IntercomPartyTime
//
//  Created by Matthew Hanlon on 8/4/16.
//  Copyright © 2016 Q.I. Software. All rights reserved.
//

#import "UserFinder.h"
const double kEarthRadiusInKM = 6371.00;

@interface UserFinder ()

@property (readwrite, strong) NSMutableArray* loadedUsers;
@property (readwrite, strong) NSMutableArray* latestNearbyUsers;

@end


@implementation UserFinder


-(instancetype)init
{
    if ( self = [super init] )
    {
        self.loadedUsers = [NSMutableArray array];
    }
    return self;
}

- (void)loadUsersFromFile
{
    NSString* customerFilePath = [[NSBundle mainBundle] pathForResource:@"customers" ofType:@"json"];
    NSString* customerFileAsString = [NSString stringWithContentsOfFile:customerFilePath encoding:NSUTF8StringEncoding error:nil]; // Error handling, what error handling?
    NSArray* customerLines = [customerFileAsString componentsSeparatedByString:@"\n"];
    for ( NSString* line in customerLines )
    {
        NSData* lineData = [line dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary* nextUser = [NSJSONSerialization JSONObjectWithData:lineData options:NSJSONReadingMutableContainers error:nil]; // Again, error handling?
        if ( nextUser != nil )
        {
            [self.loadedUsers addObject:nextUser];
        }
    }
    
}

- (double)distanceInKMForLat1:(double)lat1 long1:(double)long1 lat2:(double)lat2 long2:(double)long2
{
    double longitude_diff = ( long2 - long1 );
    double latitude_diff = ( lat2 - lat1 );
    // This marks the central angle, ala https://en.wikipedia.org/wiki/Great-circle_distance#Computational_formulas
    double a = ( pow( sin( ( latitude_diff * ( M_PI / 180 ) ) / 2 ), 2) + cos( lat1 * ( M_PI / 180 ) ) * cos( lat2 * ( M_PI / 180 ) ) * pow( sin( ( longitude_diff * ( M_PI / 180 ) ) / 2 ), 2 ) );
    double c = 2 * atan2( sqrt( a ), sqrt( 1 - a ) );
    // And then we figure the distance
    return ( c * kEarthRadiusInKM );
}

- (NSArray*)usersWithin:(double)distanceInKM ofLatitude:(double)latitude andLongitude:(double)longitude
{
    self.latestNearbyUsers = [[NSMutableArray alloc] init];
    
    for ( NSDictionary* user in self.loadedUsers )
    {
        NSNumber* userLat = user[@"latitude"];
        NSNumber* userLong = user[@"longitude"];
        if (userLat != nil && userLong != nil )
        {
            double distance = [self distanceInKMForLat1:latitude long1:longitude lat2:[userLat doubleValue] long2:[userLong doubleValue]];
            if ( distance <= distanceInKM )
            {
                NSMutableDictionary* mutableUser = [NSMutableDictionary dictionaryWithDictionary:user];
                [mutableUser setValue:[NSNumber numberWithDouble:distance] forKey:@"distance_km"];
                [self.latestNearbyUsers addObject:mutableUser];
            }
        }
    }
    return self.latestNearbyUsers;
}

- (NSArray*)nearbyUsers
{
    return self.latestNearbyUsers;
}

@end
