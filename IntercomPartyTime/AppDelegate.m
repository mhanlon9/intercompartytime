//
//  AppDelegate.m
//  IntercomPartyTime
//
//  Created by Matthew Hanlon on 8/4/16.
//  Copyright © 2016 Q.I. Software. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    return YES;
}

@end
