//
//  UserFinder.h
//  IntercomPartyTime
//
//  Created by Matthew Hanlon on 8/4/16.
//  Copyright © 2016 Q.I. Software. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserFinder : NSObject

- (void)loadUsersFromFile;
- (double)distanceInKMForLat1:(double)lat1 long1:(double)long1 lat2:(double)lat2 long2:(double)long2;
- (NSArray*)usersWithin:(double)distanceInKM ofLatitude:(double)latitude andLongitude:(double)longitude;

- (NSArray*)nearbyUsers;
@end
